<?php
	class message_model extends api_model {
		/* Get message
		 */
		public function get_message($message_id) {
			$query = "select m.* from messages m, devices d ".
			         "where m.device_id=d.id and m.id=%d and d.user_id=%d";

			if (($result = $this->db->execute($query, $message_id, $this->user->id)) === false) {
				return false;
			} else if (count($result) == 0) {
				return null;
			}

			return $result[0];
		}

		/* Valiate message id
		 */
		public function valid_message_id($message_id) {
			return valid_input($message_id, VALIDATE_NUMBERS, VALIDATE_NONEMPTY);
		}

		/* Validate device indentifier
		 */
		public function valid_device_identifier($device_identifier) {
			if ($device_identifier == null) {
				return false;
			}

			$query = "select count(*) as count from devices where user_id=%d and identifier=%s";
			if (($result = $this->db->execute($query, $this->user->id, $device_identifier)) == false) {
				return false;
			}

			return $result[0]["count"] > 0;
		}

		/* Get message index
		 */
		public function get_index($device_identifier) {
			$query = "select m.id, length(m.content) as size, m.content, m.type ".
			         "from messages m, devices d ".
			         "where m.device_id=d.id and d.identifier=%s and d.user_id=%d";

			if (($messages = $this->db->execute($query, $device_identifier, $this->user->id)) === false) {
				return false;
			}

			$types = array("invitation", "message");

			$index = array();
			foreach ($messages as $message) {
				$entry = array(
					"id"   => (int)$message["id"],
					"size" => (int)$message["size"],
					"type" => $types[$message["type"]]);
				array_push($index, $entry);
			}

			return $index;
		}

		/* Get user id by alias
		 */
		public function get_user_id($alias) {
			if ($alias == null) {
				return false;
			}

			$query = "select user_id from aliases where alias=%s";
			if (($result = $this->db->execute($query, $alias)) == false) {
				return false;
			}

			return (int)$result[0]["user_id"];
		}

		/* Delete messages
		 */
		public function delete_messages($message_id) {
			if ($this->get_message($message_id) == false) {
				return false;
			}

			$query = "delete from messages where id=%d";
			if ($this->db->query($query, $message_id) === false) {
				return false;
			} else if ($this->db->affected_rows == 0) {
				return null;
			}

			return true;
		}

		/* Save transit message
		 */
		public function save_transit_message($message) {
			list($username, $hostname) = explode("@", $message["receiver"]);

			$data = array(
				"id"        => null,
				"hostname"  => $hostname,
				"received"  => date("Y-m-d H:i:s"),
				"attempts"  => 0,
				"receiver"  => $username,
				"message"   => $message["message"],
				"signature" => $message["signature"]);

			return $this->db->insert("transit", $data) !== false;
		}
	}
?>
