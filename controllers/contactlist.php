<?php
	class contactlist_controller extends api_controller {
		public function post() {
			$fields = array(
				"receiver"   => "s1",
				"sender"     => "s1",
				"name"       => "s1",
				"alias"      => "s1",
				"public_key" => "s2",
				"dh_value"   => "s1",
				"signature"  => "s2",
				"group_id"   => "n4");

			if (($message = $this->model->create_message($_POST, $fields)) == false) {
				$this->set_error(400, "Incorrect or incomplete invitation request.");
			} else if (($user_id = $this->model->get_user_id($_POST["receiver"])) === false) {
				$this->set_error(DEBUG_MODE == "yes" ? 404 : 201);
			} else if ($this->model->save_message($message, MSG_TYPE_INVITATION, $user_id) == false) {
				$this->set_error(500);
			} else {
				$this->set_error(201);
			}
		}
	}
?>
