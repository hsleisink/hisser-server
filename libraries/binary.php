<?php
	/* libraries/http.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 */

	class binary {
		private $format = null;
		private $multi = null;
		private $elements = array();
		private $code = array(null, "C", "n", null, "N");
		private $labels = null;

		/* Constructor
		 */
		public function __construct($format, $multi = 0) {
			$this->format = str_split($format, 2);

			if (in_array($multi, array(0, 1, 2, 4)) == false) {
				$this->multi = 4;
			} else {
				$this->multi = $multi;
			}

			$this->labels = range(0, count($this->format));
		}

		/* Add element
		 */
		public function add_element() {
			if (($this->multi == 0) && (count($this->elements) > 0)) {
				return false;
			}

			if (func_num_args() != count($this->format)) {
				return false;
			}

			$args = func_get_args();
			$max = array(null, 255, 65535, null, 4294967295);

			foreach ($this->format as $nr => $item) {
				list($type, $length) = str_split($item, 1);

				if (in_array($length, array(1, 2, 4)) == false) {
					return false;
				}

				switch ($type) {
					case "n":
						if (is_int($args[$nr]) == false) {
							return false;
						}
						if ($args[$nr] > $max[$length]) {
							return false;
						}
						break;
					case "s":
						if (is_string($args[$nr]) == false) {
							return false;
						}
						if (strlen($args[$nr]) > $max[$length]) {
							return false;
						}
						break;
					default:
						return false;
				}
			}

			array_push($this->elements, $args);

			return true;
		}

		/* Encode data to binary string
		 */
		public function encode() {
			if (($elements = count($this->elements)) == 0) {
				return false;
			}

			$result = "";

			if ($this->multi > 0) {
				$result .= pack($this->code[$this->multi], $elements);
			}

			foreach ($this->elements as $element) {
				foreach ($this->format as $nr => $item) {
					list($type, $length) = str_split($item, 1);

					switch ($type) {
						case "n":
							$result .= pack($this->code[$length], $element[$nr]);
							break;
						case "s":
							$result .= pack($this->code[$length], strlen($element[$nr])) . $element[$nr];
							break;
					}
				}
			}

			return $result;
		}

		/* Set labels for decoding
		 */
		public function set_labels() {
			$labels = func_get_args();

			if (count($labels) != count($this->format)) {
				return false;
			}

			$this->labels = $labels;

			return true;
		}

		/* Decode binary string to array
		 */
		public function decode($data) {
			if ($this->multi == 0) {
				$elements = 1;
			} else {
				$elements = unpack($this->code[$this->multi], substr($data, 0, $this->multi));
				$elements = array_shift($elements);
				$data = substr($data, $this->multi);
			}

			$result = array();

			while ($elements-- > 0) {
				$element = array();

				foreach ($this->format as $nr => $item) {
					list($type, $length) = str_split($item, 1);

					if ($data == "") {
						return false;
					}

					switch ($type) {
						case "n":
							$value = unpack($this->code[$length], substr($data, 0, $length));
							$value = array_shift($value);
							$element[$this->labels[$nr]] = $value;
							$data = substr($data, $length);
							break;
						case "s":
							$len = unpack($this->code[$length], substr($data, 0, $length));
							$len = array_shift($len);
							$element[$this->labels[$nr]] = substr($data, $length, $len);
							$data = substr($data, $length + $len);
							break;
					}
				}

				array_push($result, $element);
			}

			if ($this->multi == 0) {
				return $result[0];
			}

			return $result;
		}
	}
?>
