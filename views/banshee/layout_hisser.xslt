<?xml version="1.0" ?>
<xsl:stylesheet	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="layout_hisser">
<html>

<head>
<meta http-equiv="Content-Language" content="{language}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="author" content="Hugo Leisink" />
<meta name="publisher" content="Hugo Leisink" />
<meta name="copyright" content="Copyright (C) by Hugo Leisink. All rights reserved. Protected by the Copyright laws of the Netherlands and international treaties." />
<meta name="description" content="{description}" />
<meta name="keywords" content="{keywords}" />
<meta name="generator" content="Banshee PHP framework v{/output/banshee_version} (http://www.banshee-php.org/)" />
<link rel="apple-touch-icon" href="/images/iphone.png" />
<title><xsl:if test="title/@page!=''"><xsl:value-of select="title/@page" /> - </xsl:if><xsl:value-of select="title" /></title>
<xsl:for-each select="alternates/alternate">
<link rel="alternate" title="{.}"  type="{@type}" href="{@url}" />
</xsl:for-each>
<link rel="stylesheet" type="text/css" href="/css/banshee/layout_hisser.css" />
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="{.}" />
</xsl:for-each>
<xsl:if test="inline_css!=''">
<style type="text/css">
<xsl:value-of select="inline_css" />
</style>
</xsl:if>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="{.}"></script><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<xsl:if test="javascripts/@onload">
	<xsl:attribute name="onLoad">javascript:<xsl:value-of select="javascripts/@onload" /></xsl:attribute>
</xsl:if>
<div class="gradient"></div>
<div class="wrapper">
	<div class="header">
		<div class="title"><xsl:value-of select="/output/layout_hisser/title" /></div>
		<div class="subtitle">Server web interface</div>
		<div class="menu">
			<ul>
				<xsl:for-each select="/output/menu/item">
				<li><xsl:if test="@current='yes'"><xsl:attribute name="class">current</xsl:attribute></xsl:if><a href="{link}"><xsl:value-of select="text" /></a></li>
				</xsl:for-each>
			</ul>
		</div>
	</div>
	<div class="page">
		<div class="content">
			<xsl:if test="/output/content/blocks/sidebar">
				<div class="sidebar">
				<h3><xsl:value-of select="/output/content/blocks/sidebar/title" /></h3>
				<xsl:value-of disable-output-escaping="yes" select="/output/content/blocks/sidebar/content" />
				</div>
			</xsl:if>
			<xsl:apply-templates select="/output/system_messages" />
			<xsl:apply-templates select="/output/system_warnings" />
			<xsl:apply-templates select="/output/content" />
		</div>
	</div>
	<div class="footer">
		<span>Hisser server v<xsl:value-of select="/output/hisser_version" /></span>
		<xsl:if test="/output/user"><span>Logged in as <a href="/session"><xsl:value-of select="/output/user" /></a></span></xsl:if>
		<span><a href="/apikey">Request API key</a></span>
		<span><a href="/admin">CMS</a></span>
		<xsl:if test="/output/user"><span><a href="/logout">Logout</a></span></xsl:if>
	</div>
</div>
<xsl:apply-templates select="/output/internal_errors" />
</body>

</html>
</xsl:template>

</xsl:stylesheet>
