<?php
	/*libraries/api_model.php
	 */

	abstract class api_model extends model {
		/* Check account expiry date
		 */
		public function account_expired() {
			if ($this->user->logged_in == false) {
				return false;
			}

			if ($this->user->expire_date == null) {
				return false;
			}

			if (strtotime($this->user->expire_date. "00:00:00") > time()) {
				return false;
			}

			return true;
		}

		/* Create message from user's input
		 */
		public function create_message($data, $fields) {
			$missing = array_diff(array_keys($fields), array_keys($data));
			if (count($missing) > 0) {
				return false;
			}

			$format = implode("", $fields);

			$element = array();
			foreach ($fields as $key => $value) {
				if ($value[0] == "n") {
					$data[$key] = (int)$data[$key];
				}
				array_push($element, $data[$key]);
			}

			$binary = new binary($format);
			call_user_func_array(array($binary, "add_element"), $element);
			if (($result = $binary->encode()) === false) {
				return false;
			}

			return base64_encode($result);
		}

		/* Get user id by username
		 */
		private function get_user_id($username) {
			static $cache = array();

			if (isset($cache[$username]) == false) {
				if (($user = $this->db->entry("users", $username, "username")) == false) {
					return false;
				}
				$cache[$username] = (int)$user["id"];
			}

			return $cache[$username];
		}

		/* Check username validity
		 */
		public function valid_user($username) {
			return $this->get_user_id($username) !== false;
		}

		/* Save message to user's message queue
		 */
		public function save_message($message, $type, $user_id) {
			$query = "select * from devices where user_id=%d";
			if (($devices = $this->db->execute($query, $user_id)) == false) {
				return true;
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			$origin = hash("sha256", $_SERVER["REMOTE_ADDR"].$this->settings->secret_website_code);

			foreach ($devices as $device) {
				$data = array(
					"id"        => NULL,
					"device_id" => $device["id"],
					"content"   => $message,
					"type"      => $type,
					"origin"    => $origin);

				if ($this->db->insert("messages", $data) == false) {
					$this->db->query("rollback");
					return false;
				}
			}

			if ($this->db->query("commit") === false) {
				return false;
			}

			/* Send notification
			 */
			if (($this->settings->notification_api_key != "") || ($this->settings->notification_server != "")) {
				$notification_server = new HTTPS($this->settings->notification_server);
				foreach ($devices as $device) {
					if (($device["notify_type"] == "none") || ($device["notify_token"] == "")) {
						continue;
					}

					$data = array(
						"notify_type"  => $device["notify_type"],
						"notify_token" => $device["notify_token"],
						"api_key"      => $this->settings->notification_api_key);

					$result = $notification_server->POST("/notify", $data);
				}
			}

			return true;
		}
	}
?>
