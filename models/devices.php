<?php
	class devices_model extends model {
		private $notify_types = array("none", "apns", "email", "nma", "prowl");

		public function __get($key) {
			switch ($key) {
				case "notify_types": return $this->notify_types;
			}

			return null;
		}

		public function get_devices() {
			$query = "select * from devices where user_id=%d order by id";

			return $this->db->execute($query, $this->user->id);
		}

		public function get_device($device_id) {
			$query = "select * from devices where id=%d and user_id=%d";

			if (($result = $this->db->execute($query, $device_id, $this->user->id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function save_oke($device) {
			$result = true;

			if ($this->get_device($device["id"]) == false) {
				$this->output->add_message("Device not found.");
				$result = false;
			} else {
				if (trim($device["name"]) == "") {
					$this->output->add_message("Invalid device name.");
					$result = false;
				}

				if (in_array($device["notify_type"], $this->notify_types) == false) {
					$this->output->add_message("Invalid device type.");
					$result = false;
				}
			}

			return $result;
		}

		public function update_device($device) {
			$keys = array("name", "notify_type", "notify_token");

			return $this->db->update("devices", $device["id"], $device, $keys);
		}

		public function delete_oke($device_id) {
			$result = true;

			if ($this->get_device($device_id) == false) {
				$this->output->add_message("Device not found.");
				$result = false;
			}

			return $result;
		}

		public function delete_device($device_id) {
			$queries = array(
				array("delete from messages where device_id=%d", $device_id),
				array("delete from devices where id=%d", $device_id));

			return $this->db->transaction($queries);
		}
	}
?>
