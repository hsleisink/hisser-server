<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />
<xsl:include href="../banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="list">
<tr>
<th>Hostname</th>
<th>Name</th>
<th>E-mail address</th>
<th>Last IP address</th>
<th>Approved</th>
</tr>
<xsl:for-each select="apikeys/apikey">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="hostname" /></td>
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="email" /></td>
<td><xsl:value-of select="last_ip" /></td>
<td><xsl:value-of select="approved" /></td>
</tr>
</xsl:for-each>
</table>
<xsl:apply-templates select="pagination" />

<!--a href="/{/output/page}/new" class="button">New apikey</a-->
<a href="/admin" class="button">Back</a>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="apikey/@id">
<input type="hidden" name="id" value="{apikey/@id}" />
</xsl:if>

<table class="edit">
<tr><td>API key:</td><td><xsl:value-of select="apikey/api_key" /></td></tr>
<tr><td>Hostname:</td><td><xsl:value-of select="apikey/hostname" /></td></tr>
<tr><td>Name:</td><td><xsl:value-of select="apikey/name" /></td></tr>
<tr><td>E-mail address:</td><td><a href="mailto:{apikey/email}"><xsl:value-of select="apikey/email" /></a></td></tr>
<tr><td>Last IP address:</td><td><xsl:value-of select="apikey/last_ip" /></td></tr>
<tr><td>Approved:</td><td><xsl:value-of select="apikey/approved" /></td></tr>
</table>

<xsl:if test="apikey/approved='no'">
<input type="submit" name="submit_button" value="Approve API key" class="button" onClick="javascript:return confirm('APPROVE: Are you sure?')" />
</xsl:if>
<a href="/{/output/page}" class="button">Cancel</a>
<xsl:if test="apikey/@id">
<input type="submit" name="submit_button" value="Delete API key" class="button" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>API key administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
