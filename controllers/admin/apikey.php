<?php
	class admin_apikey_controller extends controller {
		private function show_overview() {
			if (($apikey_count = $this->model->count_apikeys()) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$paging = new pagination($this->output, "apikeys", $this->settings->admin_page_size, $apikey_count);

			if (($apikeys = $this->model->get_apikeys($paging->offset, $paging->size)) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$this->output->open_tag("overview");

			$this->output->open_tag("apikeys");
			foreach ($apikeys as $apikey) {
				$apikey["approved"] = show_boolean($apikey["approved"]);
				$this->output->record($apikey, "apikey");
			}
			$this->output->close_tag();

			$paging->show_browse_links();

			$this->output->close_tag();
		}

		private function show_apikey_form($apikey) {
			$this->output->open_tag("edit");
			$apikey["approved"] = show_boolean($apikey["approved"]);
			$this->output->record($apikey, "apikey");
			$this->output->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Approve API key") {
					/* Save apikey
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_apikey_form($_POST);
#					} else if (isset($_POST["id"]) === false) {
#						/* Create apikey
#						 */
#						if ($this->model->create_apikey($_POST) === false) {
#							$this->output->add_message("Error creating apikey.");
#							$this->show_apikey_form($_POST);
#						} else {
#							$this->user->log_action("apikey created");
#							$this->show_overview();
#						}
					} else {
						/* Update apikey
						 */
						if ($this->model->update_apikey($_POST) === false) {
							$this->output->add_message("Error updating apikey.");
							$this->show_apikey_form($_POST);
						} else {
							$this->model->send_apikey_to_client($_POST["id"]);
							$this->user->log_action("apikey updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete API key") {
					/* Delete apikey
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_apikey_form($_POST);
					} else if ($this->model->delete_apikey($_POST["id"]) === false) {
						$this->output->add_message("Error deleting apikey.");
						$this->show_apikey_form($_POST);
					} else {
						$this->user->log_action("apikey deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->pathinfo[1] === "new") {
				/* New apikey
				 */
				$apikey = array();
				$this->show_apikey_form($apikey);
			} else if (valid_input($this->page->pathinfo[2], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit apikey
				 */
				if (($apikey = $this->model->get_apikey($this->page->pathinfo[2])) === false) {
					$this->output->add_tag("result", "apikey not found.\n");
				} else {
					$this->show_apikey_form($apikey);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
