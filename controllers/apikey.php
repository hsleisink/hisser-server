<?php
	class apikey_controller extends controller {
		private function show_request_form($request) {
			$this->output->record($request, "request");
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->valid_request($_POST) == false) {
					$this->show_request_form($_POST);
				} else if ($this->model->save_request($_POST) == false) {
					$this->output->add_message("Error requesting API key.");
					$this->show_request_form($_POST);
				} else {
					$this->model->notify_admins_by_email($_POST);
					$params = array("url" => "", "seconds" => 5);
					$this->output->add_tag("result", "An API key has been requested.", $params);
				}
			} else {
				$data = array();
				$this->show_request_form($data);
			}
		}
	}
?>
