<?php
	class notify_controller extends api_controller {
		public function post() {
			if ($this->model->valid_notification($_POST["notify_type"], $_POST["notify_token"]) == false) {
				$this->set_error(400, "Invalid devide token.");
			} else if ($this->model->valid_api_key($_POST["api_key"]) == false) {
				$this->set_error(403, "Invalid API key.");
			} else if ($this->model->send_notification($_POST["notify_type"], $_POST["notify_token"]) == false) {
				$this->set_error(500);
			}
		}
	}
?>
