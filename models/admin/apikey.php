<?php
	class admin_apikey_model extends model {
		public function count_apikeys() {
			$query = "select count(*) as count from api_keys order by id";

			if (($result = $this->db->execute($query)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_apikeys($offset, $limit) {
			$query = "select * from api_keys order by approved,hostname limit %d,%d";

			return $this->db->execute($query, $offset, $limit);
		}

		public function get_apikey($apikey_id) {
			return $this->db->entry("api_keys", $apikey_id);
		}

		public function save_oke($apikey) {
			$result = true;

			return $result;
		}

		public function create_apikey($apikey) {
			$keys = array("id", "api_key", "name", "email", "last_ip", "approved");

			$apikey["id"] = null;
			$apikey["last_ip"] = null;

			return $this->db->insert("api_keys", $apikey, $keys);
		}

		public function update_apikey($apikey) {
			$keys = array("approved");

			$apikey["approved"] = 1;

			return $this->db->update("api_keys", $apikey["id"], $apikey, $keys) !== false;
		}

		public function send_apikey_to_client($apikey_id) {
			if (($message = file_get_contents("../extra/apikey_approved.txt")) === false) {
				return false;
			}

			if (($request = $this->db->entry("api_keys", $apikey_id)) == false) {
				return false;
			}

			$replace = array(
				"HOSTNAME" => $_SERVER["SERVER_NAME"],
				"NAME"     => $request["name"],
				"APIKEY"   => $request["api_key"]);

			$email = new email("Hisser API key approved", $this->settings->webmaster_email, "Hisser server");
			$email->message($message);
			$email->set_message_fields($replace);

			return $email->send($request["email"], $request["name"]);
		}

		public function delete_oke($apikey) {
			$result = true;

			return $result;
		}

		public function delete_apikey($apikey_id) {
			return $this->db->delete("api_keys", $apikey_id);
		}
	}
?>
