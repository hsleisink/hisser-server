<?php
	class admin_controller extends controller {
		private $menu = array(
			"Authentication & authorization" => array(
				"Users"         => array("admin/user", "users.png"),
				"Roles"         => array("admin/role", "roles.png"),
				"Organisations" => array("admin/organisation", "organisations.png"),
				"Access"        => array("admin/access", "access.png"),
				"User switch"   => array("admin/switch", "switch.png"),
				"Action log"    => array("admin/action", "action.png")),
			"Content" => array(
				"F.A.Q."        => array("admin/faq", "faq.png"),
				"Files"         => array("admin/file", "files.png"),
				"Menu"          => array("admin/menu", "menu.png"),
				"News"          => array("admin/news", "news.png"),
				"Pages"         => array("admin/page", "page.png"),
				"Weblog"        => array("admin/weblog", "weblog.png")),
			"System" => array(
				"API keys"       => array("admin/apikey", "apikey.png"),
				"API test"      => array("admin/apitest", "apitest.png"),
				"Settings"      => array("admin/settings", "settings.png")));

		public function execute() {
			if (($this->user->id == 1) && ($this->user->password == "dc81978db6da055269483daf70438e35332ef4b5bbab453405bb0206e03f9359")) {
				$this->output->add_system_warning("Don't forget to change the password of the admin account!");
			}

			if ($this->settings->secret_website_code == "CHANGE_ME_INTO_A_RANDOM_STRING") {
				$this->output->add_system_warning("Don't forget to change the secret_website_code setting.");
			}

			if (is_true(DEBUG_MODE)) {
				$this->output->add_system_warning("Website is running in debug mode. Set DEBUG_MODE in settings/website.conf to 'no'.");
			}

			if ($this->page->pathinfo[1] != null) {	
				$this->output->add_system_warning("The administration module '%s' does not exist.", $this->page->pathinfo[1]);
			}

			if ($this->settings->database_version != HISSER_VERSION) {
				$this->output->add_system_warning("The database is not up to date. Run the script 'database/upgrade_database'.");
			}

			if (is_false(MULTILINGUAL)) {
				unset($this->menu["Content"]["Languages"]);
			}

			$access_list = page_access_list($this->db, $this->user);
			$private_pages = config_file("private_pages");

			$this->output->open_tag("menu");

			foreach ($this->menu as $text => $section) {

				$this->output->open_tag("section", array(
					"text"  => $text,
					"class" => strtr(strtolower($text), " &", "__")));

				foreach ($section as $text => $info) {
					list($page, $icon) = $info;

					if (in_array($page, $private_pages) == false) {
						continue;
					}

					if (isset($access_list[$page])) {
						$access = show_boolean($access_list[$page] > 0);
					} else {
						$access = show_boolean(true);
					}

					$this->output->add_tag("entry", $page, array(
						"text"   => $text,
						"access" => $access,
						"icon"   => $icon));
				}

				$this->output->close_tag();
			}

			$this->output->close_tag();
		}
	}
?>
