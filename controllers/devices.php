<?php
	class devices_controller extends controller {
		private function show_overview() {
			if (($devices = $this->model->get_devices()) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$this->output->open_tag("overview");

			$this->output->open_tag("devices", array("max" => $this->settings->max_devices_per_user));
			foreach ($devices as $device) {
				$this->output->record($device, "device");
			}
			$this->output->close_tag();

			$this->output->close_tag();
		}

		private function show_device_form($device) {
			$this->output->open_tag("edit");

			$this->output->open_tag("types");
			foreach ($this->model->notify_types as $type) {
				$this->output->add_tag("type", $type);
			}
			$this->output->close_tag();

			$this->output->record($device, "device");

			$this->output->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save device") {
					/* Save device
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_device_form($_POST);
					} else {
						/* Update device
						 */
						if ($this->model->update_device($_POST) === false) {
							$this->output->add_message("Error updating device.");
							$this->show_device_form($_POST);
						} else {
							$this->user->log_action("device updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete device") {
					/* Delete device
					 */
					if ($this->model->delete_oke($_POST["id"]) == false) {
						$this->show_device_form($_POST);
					} else if ($this->model->delete_device($_POST["id"]) === false) {
						$this->output->add_message("Error deleting device.");
						$this->show_device_form($_POST);
					} else {
						$this->user->log_action("device deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if (valid_input($this->page->pathinfo[1], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit device
				 */
				if (($device = $this->model->get_device($this->page->pathinfo[1])) === false) {
					$this->output->add_tag("result", "Device not found.\n");
				} else {
					$this->show_device_form($device);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
