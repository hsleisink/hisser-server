<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Request template
//
//-->
<xsl:template match="request">
<xsl:call-template name="show_messages" />
<p>This form can be used to request an API key required for using the notification service (APNS/NMA) on this server. Enter your name, your e-mail address and the hostname of the Hisser server you will be running. After approval, the API key will be send to the e-mail address registered with the domain name of the supplied hostname.</p>
<form action="/{/output/page}" method="post">
<table>
<tr><td>Name:</td><td><input type="text" name="name" value="{name}" class="text" /></td></tr>
<tr><td>E-mail address:</td><td><input type="text" name="email" value="{email}" class="text" /></td></tr>
<tr><td>Hostname:</td><td><input type="text" name="hostname" value="{hostname}" class="text" /></td></tr>
</table>
<input type="submit" name="submit_button" value="Request API key" class="button" />
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Request API key</h1>
<xsl:apply-templates select="request" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
