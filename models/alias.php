<?php
	class alias_model extends api_model {
		/* Get all aliases for user
		 */
		public function get_aliases() {
			$query = "select * from aliases where user_id=%d";

			return $this->db->execute($query, $this->user->id);
		}

		/* Validate alias
		 */
		public function valid_alias($alias) {
			if (strlen(trim($alias)) < 10) {
				return false;
			}

			$query = "select * from aliases where alias=%s";
			if (($result = $this->db->execute($query, $alias)) === false) {	
				return false;
			}

			return count($result) == 0;
		}

		/* Check if alias already exists for user
		 */
		public function alias_exists($alias) {
			$query = "select count(*) as count from aliases ".
			         "where alias=%s and user_id=%d";

			if (($result = $this->db->execute($query, $alias, $this->user->id)) === false) {
				return true;
			}

			return $result[0]["count"] > 0;
		}

		/* Insert alias in database
		 */
		public function create_alias($alias) {
			$data = array(
				"id"      => null,
				"user_id" => (int)$this->user->id,
				"alias"   => $alias);

			return $this->db->insert("aliases", $data) !== false;
		}

		/* Remove alias from database
		 */
		public function delete_alias($alias) {
			$query = "delete from aliases where alias=%s and user_id=%d";

			if ($this->db->query($query, $alias, $this->user->id) === false) {
				return false;
			} else if ($this->db->affected_rows == 0) {
				return null;
			}

			return true;
		}
	}
?>
