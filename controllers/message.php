<?php
	class message_controller extends api_controller {
		/* Show message
		 */
		public function get() {
			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if ($this->model->account_expired()) {
				$this->set_error(402);
			} else if ($this->model->valid_message_id($_GET["id"]) == false) {
				$this->set_error(400, "Invalid message id.");
			} else if (($message = $this->model->get_message($_GET["id"])) === false) {
				$this->set_error(500);
			} else if ($message === null) {
				$this->set_error(404, "Message not foud.");
			} else {
				return base64_decode($message["content"]);
			}
		}

		/* Show message count
		 */
		public function get_index() {
			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if ($this->model->account_expired()) {
				$this->set_error(402);
			} else if ($this->model->valid_device_identifier($_GET["device"]) == false) {
				$this->set_error(403, "Invalid device token.");
			} else if (($index = $this->model->get_index($_GET["device"])) === false) {
				$this->set_error(500);
			} else if (count($index) == 0) {
				$this->set_error(204, "No messages waiting.");
			} else {
				$binary = new binary("n4n4s1", 2);
				foreach ($index as $entry) {
					$binary->add_element($entry["id"], $entry["size"], $entry["type"]);
				}

				return $binary->encode();
			}
		}

		/* Save message
		 */
		public function post() {
			$fields = array(
				"receiver"  => "s1",
				"message"   => "s4",
				"signature" => "s2");

			/* Relay message
			 */
			if (strpos($_POST["receiver"], "@") !== false) {
				list($username, $hostname) = explode("@", $_POST["receiver"]);
				if ($hostname == $_SERVER["SERVER_NAME"]) {
					$_POST["receiver"] = $username;
				} else if ($this->user->logged_in == false) {
					$this->set_error(401);
					return;
				} else if ($this->model->save_transit_message($_POST) == false) {
					$this->set_error(500);
					return;
				}
			}

			/* Save message
			 */
			if (($message = $this->model->create_message($_POST, $fields)) == false) {
				$this->set_error(400, "Incorrect or incomplete message.");
			} else if (($user_id = $this->model->get_user_id($_POST["receiver"])) === false) {
				$this->set_error(DEBUG_MODE == "yes" ? 404 : 201);
			} else if ($this->model->save_message($message, MSG_TYPE_MESSAGE, $user_id) == false) {
				$this->set_error(500);
			} else {
				$this->set_error(201);
			}
		}

		/* Delete message
		 */
		public function post_delete() {
			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if ($this->model->valid_message_id($_POST["id"]) == false) {
				$this->set_error(400, "Invalid message message id.");
			} else if (($result = $this->model->delete_messages($_POST["id"])) === false) {
				$this->set_error(500);
			} else if ($result === null) {
				$this->set_error(404, "Message not found.");
			}
		}
	}
?>
