<?php
	class contactlist_model extends api_model {
		/* Get user id by username
		 */
		public function get_user_id($username) {
			if ($username == null) {
				return false;
			}

			$query = "select id from users where username=%s";
			if (($result = $this->db->execute($query, $username)) == false) {
				return false;
			}

			return (int)$result[0]["id"];
		}
	}
?>
