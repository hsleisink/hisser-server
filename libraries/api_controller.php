<?php
	/* libraries/api_controller.php
	 */

	abstract class api_controller extends controller {
		private $http_status = 200;
		private $error_message = null;

		protected function set_error($code, $message = null) {
			$this->http_status = $code;

			if ($message === null) {
				if ($code == 401) {
					$message = "Authentication required.";
				} else if ($code == 402) {
					$message = "Account expired.";
				} else if ($code == 500) {
					$message = "Internal server error.";
				}
			}

			if ($message !== null) {
				$this->error_message = "error: ".rtrim($message)."\n";
			}

		}

		public function execute() {
			$this->output->disable();

			$function = strtolower($_SERVER["REQUEST_METHOD"]);
			if (count($this->page->parameters) > 0) {
				$uri_part = "_".implode("_", $this->page->parameters);
				$function .= $uri_part;
			}

			if (method_exists($this, $function)) {
				if (($result = call_user_func(array($this, $function))) === null) {
					$result = $this->error_message;
				}
			} else {
				$methods = array_diff(array("GET", "POST", "PUT", "DELETE"), array($_SERVER["REQUEST_METHOD"]));
				$allowed = array();
				foreach ($methods as $method) {
					if (method_exists($this, strtolower($method).$uri_part)) {
						array_push($allowed, $method);
					}
				}

				if (count($allowed) == 0) {
					$this->set_error(404, "Unknown API call.");
				} else {
					$this->set_error(405, "Method not allowed.");
					header("Allowed: ".implode(", ", $allowed));
				}

				$result = $this->error_message;
			}

			if (is_array($result)) {
				header("Content-Type: application/json");
				$result = json_encode($result);
			} else if ($this->http_status == 200) {
				header("Content-Type: application/octet-stream");
			} else {
				header("Content-Type: text/plain");
			}

			if ($this->http_status != 200) {
				header(sprintf("Status: %d", $this->http_status));
				if ($this->http_status == 401) {
					header("WWW-Authenticate: Basic realm=\"Hisser server web interface\"");
				}
			}

			if ($result !== null) {
				print $result;
			}
		}
	}
?>
