<?php
	class register_controller extends api_controller {
		public function post() {
			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if ($this->model->account_expired()) {
				$this->set_error(402);
			} else if ($this->model->valid_registration($_POST) == false) {
				$this->set_error(400, "Incorrect type or token.");
			} else if ($this->model->registration_allowed($_POST) == false) {
				$this->set_error(403, "Maximum device count reached.");
			} else if ($this->model->save_registration($_POST) == false) {
				$this->set_error(500);
			} else {
				$this->set_error(201);
			}
		}
	}
?>
