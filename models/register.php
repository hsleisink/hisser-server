<?php
	class register_model extends api_model {
		public function valid_registration($registration) {
			if (trim($registration["device"]) == "") {
				return false;
			}

			$token = $registration["notify_token"];
			switch ($registration["notify_type"]) {
				case "apns":
					return strlen($token) == 64;
				case "email":
					return valid_email($token);
				case "nma":
					return strlen($token) == 64;
				case "prowl":
					return strlen($token) == 40;
				case "none":
					return $token == "";
			}

			return false;
		}

		private function get_devices() {
			static $cache = null;

			if ($cache === null) {
				$query = "select * from devices where user_id=%d";
				$cache = $this->db->execute($query, $this->user->id);
			}

			return $cache;
		}

		private function already_registered($device_id) {
			if (($devices = $this->get_devices()) === false) {
				return null;
			}

			foreach ($devices as $device) {
				if ($device["identifier"] == $device_id) {
					return true;
				}
			}

			return false;
		}

		public function registration_allowed($registration) {
			if ($this->already_registered($registration["device"]) === true) {
				return true;
			}

			if (($devices = $this->get_devices()) === false) {
				return false;
			}

			if (count($devices) >= $this->settings->max_devices_per_user) {
				return false;
			}

			return true;
		}

		public function save_registration($registration) {
			$already_registered = $this->already_registered($registration["device"]);

			if ($already_registered === null) {
				return false;
			} else if ($already_registered === false) {
				$keys = array("id", "user_id", "identifier", "name", "notify_type", "notify_token");
				
				$registration["id"] = null;
				$registration["user_id"] = (int)$this->user->id;
				$registration["identifier"] = $registration["device"];
				if (($registration["name"] = $_SERVER["HTTP_USER_AGENT"]) == null) {
					$registration["name"] = "Unknown device";
				}

				return $this->db->insert("devices", $registration, $keys) !== false;
			} else if ($already_registered === true) {
				$query = "update devices set notify_type=%s, notify_token=%s ".
				         "where user_id=%d and identifier=%s";
				return $this->db->query($query, $registration["notify_type"], $registration["notify_token"],
				                                $this->user->id, $registration["device"]);
			}
		}
	}
?>
