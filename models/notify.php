<?php
	class notify_model extends api_model {
		public function valid_notification($type, $token) {
			return (strlen($type) > 0) && (strlen($token) > 0);
		}

		public function valid_api_key($api_key) {
			$query = "select * from api_keys where api_key=%s";

			if (($result = $this->db->execute($query, $api_key)) == false) {
				return false;
			}

			if ($result[0]["last_ip"] != $_SERVER["REMOTE_ADDR"]) {
				$query = "update api_keys set last_ip=%s where api_key=%s";
				$this->db->query($query, $_SERVER["REMOTE_ADDR"], $api_key);

				$message = "New IP address: ".$api_key."\n";

				$email = new email("Notify API user changed IP address", $this->settings->webmaster_email);
				$email->message($message);
				$email->send($this->settings->webmaster_email);
			}

			return true;
		}

		public function send_notification($notify_type, $notify_token) {
			$application = "Hisser";
			$event = "New Hisser message";
			$message = "You've recieved a new Hisser message.";

			switch ($notify_type) {
				case "apns":
					$apns = new APNS("../extra/hisser_apn_distribution.pem");
					return $apns->send_notification($notify_token, $event);
				case "email":
					$email = new email($event, $this->settings->webmaster_email, "Hisser server");
					$email->message($message);
					return $email->send($notify_token);
				case "nma":
					$nma = new NMA($application, $notify_token);
					return $apns->send_notification($event, $message);
				case "prowl":
					$prowl = new Prowl($application, $notify_token);
					return $prowl->send_notification($event, $message);
			}

			return false;
		}
	}
?>
