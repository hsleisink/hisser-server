<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="list">
<tr>
<th>Name</th>
<th>Device identifier</th>
<th>Notify type</th>
</tr>
<xsl:for-each select="devices/device">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="identifier" /></td>
<td><xsl:value-of select="notify_type" /></td>
</tr>
</xsl:for-each>
</table>
<p>You can have up to <xsl:value-of select="devices/@max" /> device(s) with this account.</p>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="device/@id">
<input type="hidden" name="id" value="{device/@id}" />
</xsl:if>

<table class="edit">
<tr><td>Name:</td><td><input type="text" name="name" value="{device/name}" class="text" /></td></tr>
<tr><td>Identifier:</td><td><xsl:value-of select="device/identifier" /></td></tr>
<tr><td>Notify type:</td><td><select name="notify_type" class="text">
<xsl:for-each select="types/type">
<option><xsl:if test=".=../../device/notify_type"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select></td></tr>
<tr><td>Notify key:</td><td><input type="text" name="notify_token" value="{device/notify_token}" class="text" /></td></tr>
</table>

<input type="submit" name="submit_button" value="Save device" class="button" />
<a href="/{/output/page}" class="button">Cancel</a>
<xsl:if test="device/@id">
<input type="submit" name="submit_button" value="Delete device" class="button" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Devices</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
