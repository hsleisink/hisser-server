<?php
	$allowed_uploads = array("jpg", "jpeg", "gif", "png", "pdf", "doc", "docx", "xls", "xlsx", "txt", "rtf");
	$supported_languages = array("en" => "English");

	$months_of_year = array("january", "february", "march", "april", "may", "june",
		"july", "august", "september", "october", "november", "december");
	$days_of_week = array("monday", "tuesday", "wednesday", "thursday", "friday",
		"saturday", "sunday");
?>
