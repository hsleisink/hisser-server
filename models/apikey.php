<?php
	class apikey_model extends model {
		public function valid_request($request) {
			$result = true;

			$keys = array(
				"name"     => "your name",
				"email"    => "your e-mail address",
				"hostname" => "the hostname");

			foreach ($keys as $key => $label) {
				if (trim($request[$key]) == "") {
					$this->output->add_message("Please, fill in ".$label);
					$result = false;
				}
			}

			return $result;
		}

		public function save_request($request) {
			$keys = array("id", "api_key", "name", "email", "hostname", "last_ip", "approved");

			$request["id"] = null;
			$request["api_key"] = random_string(50);
			$request["last_ip"] = $_SERVER["REMOTE_ADDR"];
			$request["approved"] = 0;

			return $this->db->insert("api_keys", $request, $keys) !== false;
		}

		public function notify_admins_by_email($request) {
			$query = "select u.* from users u, user_role r where u.id=r.user_id and r.role_id=%d";
			if (($admins = $this->db->execute($query, ADMIN_ROLE_ID)) == false) {
				return false;
			}

			if (($message = file_get_contents("../extra/apikey_request.txt")) === false) {
				return false;
			}

			$replace = array(
				"HOSTNAME" => $_SERVER["SERVER_NAME"],
				"TITLE"    => $this->settings->head_title,
				"NAME"     => $request["name"],
				"EMAIL"    => $request["email"],
				"SERVER"   => $request["hostname"]);

			$email = new email("New API key request", $this->settings->webmaster_email, "Hisser server");
			$email->message($message);
			$email->set_message_fields($replace);

			foreach ($admins as $admin) {
				$email->to($admin["email"], $admin["fullname"]);
			}

			return $email->send();
		}
	}
?>
