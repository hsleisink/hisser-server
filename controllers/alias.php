<?php
	class alias_controller extends api_controller {
		/* Show all alliases for user
		 */
		public function get() {
			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if (($aliases = $this->model->get_aliases()) === false) {
				$this->set_error(500);
			} else if (count($aliases) == 0) {
				$this->set_error(204, "No aliases found.");
			} else {
				$binary = new binary("s1", 2);
				foreach ($aliases as $alias) {
					$binary->add_element($alias["alias"]);
				}

				return $binary->encode();
			}
		}

		/* Add alias to list
		 */
		public function post_create() {
			$alias = $_POST["alias"];

			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if ($this->model->account_expired()) {
				$this->set_error(402);
			} else if ($this->model->valid_alias($alias) == false) {
				$this->set_error(400, "Incorrect alias.");
			} else if ($this->model->alias_exists($alias)) {	
				$this->set_error(409, "Alias already exists.");
			} else if ($this->model->create_alias($alias) == false) {
				$this->set_error(500);
			} else {
				$this->set_error(201);
			}
		}

		/* Remove alias from list
		 */
		public function post_delete() {
			$alias = $_POST["alias"];

			if ($this->user->logged_in == false) {
				$this->set_error(401);
			} else if ($this->model->valid_alias($alias) == false) {
				$this->set_error(400, "Invalid alias");
			} else if (($result = $this->model->delete_alias($alias)) === false) {
				$this->set_error(500);
			} else if ($result === null) {
				$this->set_error(404, "Alias not found.");
			}
		}
	}
?>
